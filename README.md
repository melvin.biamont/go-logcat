# Go-Logcat

## What is it ?

Go-Logcat is a very small logger for Go.
It allows to separate logs by severity.

## How to import it ?

```bash
go get gitlab.com/melvin.biamont/go-logcat
```

## How to use it ?

```go

// If you just want to print a Verbose message.
logcat.V("MyTag", "My message")

// If you just want to print a Debug message.
logcat.D("MyTag", aStruct)

// If you just want to print a Information message.
logcat.I("MyTag", "My message")

// If you just want to print a Warning message.
logcat.W("MyTag", "My warning")

// If you want to print an Error message.
locat.E("MyTag", err)

// If you want to print an Assertion message.
locat.A("MyTag", err)
```