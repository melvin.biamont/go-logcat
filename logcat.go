package logcat

import (
	"log"
	"os"
)

func V(tag string, msg interface{}) {
	log.Printf("%d V/%s: %s\n", os.Getpid(), tag, msg)
}

func D(tag string, msg interface{}) {
	log.Printf("%d D/%s: %s\n", os.Getpid(), tag, msg)
}

func I(tag string, msg interface{}) {
	log.Printf("%d I/%s: %s\n", os.Getpid(), tag, msg)
}

func W(tag string, msg interface{}) {
	log.Printf("%d W/%s: %s\n", os.Getpid(), tag, msg)
}

func E(tag string, msg interface{}) {
	log.Printf("%d E/%s: %s\n", os.Getpid(), tag, msg)
}

func A(tag string, msg interface{}) {
	log.Printf("%d A/%s: %s\n", os.Getpid(), tag, msg)
}